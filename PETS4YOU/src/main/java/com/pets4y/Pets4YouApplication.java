package com.pets4y;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pets4YouApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pets4YouApplication.class, args);
	}

}
